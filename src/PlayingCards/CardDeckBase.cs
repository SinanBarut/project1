﻿namespace PlayingCards
{
    public class CardDeckBase<T> where T : CardBase, new()
    {
        public CardDeckBase()
        {
            Cards = new T[52];
        }
        public T[] Cards { get; set; }
    }
}
