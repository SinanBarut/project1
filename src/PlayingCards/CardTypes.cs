﻿namespace PlayingCards
{
    public enum CardTypes
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades,
    }
}
