﻿namespace PlayingCards
{
    public class CardBase
    {
        public CardTypes CardType { get; set; }
        public CardRanks CardRank { get; set; }
    }
}
