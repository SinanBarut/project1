﻿using System;

namespace Permutation
{
    class Program
    {
        static int Main(string[] args)
        {
            if(args.Length != 2)
            {
                Console.WriteLine("Lütfen 2 farklı string giriniz.");
                return 1;
            }
           
            Console.WriteLine($"args[0] : {args[0]}");
            Console.WriteLine($"args[1] : {args[1]}");
            Console.WriteLine($"Result : {HasPermutation(args[0], args[1])}!");

            return 0;
        }

        private static bool HasPermutation(string sentence1, string sentence2)
        {
            if (string.IsNullOrEmpty(sentence1) || string.IsNullOrEmpty(sentence2))
            {
                // Basit bir çözüm olarak sadece false döndürüldü. Error handling yapıldığında ArgumentNullException fırlatılması daha doğru olur
                return false;
            }

            return sentence1.Length >= sentence2.Length ? SentencePermutationControl(sentence1, sentence2) : SentencePermutationControl(sentence2, sentence1);

        }

        private static bool SentencePermutationControl(string longSentence, string shortSentence)
        {
            foreach (var item in shortSentence)
            {
                var index = longSentence.IndexOf(item);
                if (index >= 0)
                    longSentence = longSentence.Remove(index, 1);
                else
                    return false;
            }
            return true;
        }
    }
}
