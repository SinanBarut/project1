# Projenin amacı
Proje verilen 2 ifadeyi, biri diğerinin permutasyonu mu diye kontrol ediyor.

# Yüklenmesi ve çalıştırılması

## Gereksinimler
.Net Core 3.1

## Çalıştırılması
```
../Permutation #> dotnet run baba abab
../Permutation #> dotnet run baba abc
../Permutation #> dotnet run lds loodos
```

# Nasıl çalışır?
Permutasyon, ifadenin içinde bulunan elemanların n tanesinin farklı sıralamayla seçilesi sonucu oluşuyor. O zaman 2 ifade içinde eleman sayısı fazla olan bizim ana ifademiz(A diyelim) diğer ifade(B diyelim) ise permutasyon kontrolü yapacağımız ifade olacak diyebiliriz. Bu durumda B ifadesinde olan karakterin hepsi A içinde olmak zorunda. Dolayısıyla B'deki ifadeleri sırasıyla seçip A'da olup olmadığını kontrol edersem sonuca ulaşabilirim. Tabii, kontrol ettiğim ifadeyi A'nın içinden silmem gerekiyor ki tekrar etme durumlarını da kontrol edebilelim. Yani şöyle bir örnek geldiğinde sorun yaşamayalım 
```
A = "abc"
B = "aaa"
```
Bu ifade de sadece B'nin sıradaki elemanını A içinde kontrol edersem 'a' ifadesi her zaman A'nın içinde çıkacağı için doğru sonuca ulaşamam. Dolayısıyla 'a' ifadesini ilk kontrolden sonra A'nın içinden silmeliyim ki B'de bulunan 2. ve 3. 'a' ifadelerinde hata verebileyim.