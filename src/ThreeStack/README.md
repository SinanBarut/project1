# Projenin amacı
3 farklı stack'in tek bir dizi içinde nasıl tutulup yönetileceğini gösteriyor.

# Yüklenmesi ve çalıştırılması

## Gereksinimler
.Net Core 3.1

## Çalıştırılması
```
../ThreeStack #> dotnet run 
```

# Nasıl çalışır?
KStack isimli bir sınıf üretiliyor. Sınıfın içinde arr, top, next isminde 3 tane dizi,free isiminde değişken üretiliyor. Bu arrayların anlamları sırasıyla;
arr: Gönderilen elemanların tutulacağı dizi
top: Kaç farklı stack oluşturulacaksa(bu örnek için 3) onların son değerlerinin `arr` içinde kaçıncı elemanda olduğunu tutuyor.
next: Gönderilen değerin `arr` dizisine eklendiği taktirde bir bir önceki elemanın dizinin hangi sırasında olduğunu gösteriyor.
free: En son boş alanın hangisi olduğunu gösteriyor. Eğer dizideki bütün alanlar dolduysa -1 atanıyor ve dizinin dolduğunu belirtiyor.
